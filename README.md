# Zonage Eurocode 1 France - Action du vent 

Ce projet contient le tracé des différents zonages de la France métropolitaine pour les 3 coefficients liés à l'estimation des vitesses de références $v_b$, au format [GeoJSON](https://geojson.org/) : 
* $v_{b,0}$ : valeur de base de la vitesse de référence, 
* $c_{dir}$ : coefficient de réduction en fonction de la direction du vent, 
* $c_{season}$ : coefficient de réduction lié à la saison.

L'objectif est de ne plus avoir à consulter les trois cartes ci-dessous (extraites de la NF EN 1991-1-4/NA mars 2008 _Actions générales - Actions du Vent_) : 

<img src="/IMG/IM_CDIR_OLD.png"  width="250" height="250"> <img src="/IMG/IM_CSEASON_OLD.png"  width="250" height="250"> <img src="/IMG/IM_VB0_old.png"  width="250" height="250">

Mais de se concentrer sur leur "union" :

<img src="/IMG/IM_UNION_EC1.png"  width="400">

## Source des données

Projet inspiré du travail de Gregoire David : [France Geojson](https://github.com/gregoiredavid/france-geojson).

Le decoupage géographique utilisé est celui limites cantonales, selon la carte administrative de la France, publié par l'IGN - Paris 1997 (Edition 2). Le fichier de base est issu des archives de l'IGN [GEOFLA](https://geoservices.ign.fr/geofla). (lien direct découpage au format SHP : [archive](https://wxs.ign.fr/oikr5jryiph0iwhw36053ptm/telechargement/prepackage/GEOFLA_TOUSTHEMES_PACK_1997-01-01$GEOFLA_1-0__SHP_LAMB93_FXX_1997-01-01/file/GEOFLA_1-0__SHP_LAMB93_FXX_1997-01-01.7z
))

Ces données étant extrêmement précises, elles ont été simplifiées via [Mapshaper](https://mapshaper.org/) avec la méthode "VisValingam Weighted" à 25% afin d'optimiser la taille des fichiers. 

## Meta-données

Sur chaque territoire, on trouvera les propriétés suivantes : 
* `ID` : `int`, une id générique,
* `V_B0` : `int`, en [m/s], valeur de base de la vitesse de référence,
* `C_DIR_ANGL` : `str`, sous la forme `value;[minAngle, maxAngle]`, coefficient de réduction en fonction de la direction du vent,
* `C_SEASON1` : `float`, applicable de octobre à mars, coefficient de réduction lié à la saison,
* `C_SEASON2` : `float`, applicable d'avril à septembre, coefficient de réduction lié à la saison.


##  Complément

Une application [Dash](https://dash.plotly.com/) minimale est fournie afin d'explorer cette carte, construite avec [Dash Leaflet](https://dash-leaflet.herokuapp.com/):

<img src="/IMG/screen.gif"  width="600">

Pour lancer l'application :
```python
python3 minimal_app.py
```
et se connecter à `http://127.0.0.1:8050/`

Dépendances (non testé avec des versions antérieurs): 
```python
dash==2.4.0
dash_bootstrap_components==1.0.2
json==2.0.9
dash_leaflet==0.1.23
```

## Auteur

M BOGDAN - AREP L'hypercube. 

## License

[License Ouverte 2.0 / Open License 2.0](https://www.etalab.gouv.fr/licence-ouverte-open-licence/).

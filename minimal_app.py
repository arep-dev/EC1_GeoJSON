# -*- coding: utf-8 -*-
"""
december 2022
@author: Mateusz BOGDAN - AREP L'hypercube
"""
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
import dash
from dash import html, dcc
from dash.dependencies import Output, Input, State
from dash.exceptions import PreventUpdate
import dash_bootstrap_components as dbc
import json
import dash_leaflet as dl
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
with open("./ec1_windCoeff.geojson", 'r') as f:
    ec1_data = json.load(f)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
app = dash.Dash(
    __name__,external_stylesheets=[dbc.themes.BOOTSTRAP],
)
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
app.layout = html.Div([
    html.H5(
        "EC1 Actions du vent - Annexe Nationale",
        style={
            "color":"#f55654"
        }
    ),
    
    dcc.Markdown("""
                    * Utilisation : cliquez sur la carte pour obtenir :
                        * `V_B0` : `int`, en \\[m/s\\], valeur de base de la vitesse de référence,
                        * `C_DIR_ANGL` : `str`, sous la forme `value;[minAngle, maxAngle]`, coefficient de réduction en fonction de la direction du vent,
                        * `C_SEASON1` : `float`, applicable d’octobre à mars, coefficient de réduction lié à la saison,
                        * `C_SEASON2` : `float`, applicable d’avril à septembre, coefficient de réduction lié à la saison.
                         
                    * Source :
                        * NF EN 1991-1-4/NA mars 2008 Actions générales - Actions du Vent
                    * Liens :
                        * [Fichier `geojson`](https://gitlab.com/arep-dev/EC1_GeoJSON/-/blob/b6811c9516cad3fdbdd84841fe74607fd9e176f8/ec1_windCoeff.geojson)
                        * [*Repository* GitLab](https://gitlab.com/arep-dev/EC1_GeoJSON)
                """
            ),
    dbc.Row([
        dbc.Col([
            dl.Map(
                children=[
                    dl.TileLayer(),
                    dl.GeoJSON(
                        data=ec1_data,
                        options={"style":{"color":"#f55654", "opacity":"0.2"}},
                        id="geojson_ec1"),
                ],
                id="map",
                center=[47, 3],
                zoom=6,
                style={
                    'height': '600px',
                    'margin-left':'5%'
                },
            )
        ],width=6),
        dbc.Col([
            html.P("Coefficients :"),
            html.Div(id='info_ventEC1'),
        ],width=4),
        
    ]),
    html.Div(
            "❤️ L'hypercube - 2024 - Last update 18.09.2024",
            style={
                "color":"#E04E51",
                "z-index": "999",
                "font-size":"12px",
                "font-weight": "bold",
                "position":"absolute",
                "bottom":5

            }
        ),

], style={
    "margin":"10px"

})
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@app.callback(Output("info_ventEC1", "children"), [Input("geojson_ec1", "clickData")])
def click_info(data):
    if data is not None:
        print(data['properties'])
        out = json.dumps({
            'v_b,0 [m/s]': data['properties']['V_B0'],
            'c_season (oct-mars)': data['properties']['C_SEASON1'],
            'c_season (avr-sept)': data['properties']['C_SEASON2'],
            'c_dir' : float(data['properties']['C_DIR_ANGL'].split(";")[0]),
            'Secteur angulaire c_dir': data['properties']['C_DIR_ANGL'].split(";")[1:][0]
            }, indent=4)
        return html.Pre(out)
    else: 
        PreventUpdate()
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
if __name__ == '__main__':
    app.run_server(debug=True)
